﻿using Proyecto_CoffeeRun.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_CoffeeRun.Controllers
{
    public class PaginaController : Controller
    {
        private ModelCoffeeRunContainer db = new ModelCoffeeRunContainer();

        // GET: Pagina
        public ActionResult Index()
        {
            return View(db.paginaSet1.ToList());
        }

        public ActionResult Edit(int id)
        {
            var model = db.paginaSet1.SingleOrDefault(x => x.id == id);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(pagina model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }
    }
}