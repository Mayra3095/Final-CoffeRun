﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_CoffeeRun.Models;
using PagedList;
using System.Data.Entity.Core.Objects;

namespace Proyecto_CoffeeRun.Controllers
{
    public class HomeController : Controller
    {
        private ModelCoffeeRunContainer db = new ModelCoffeeRunContainer();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Index(string nombre)
        {
            ViewBag.nombre = nombre;
            ViewBag.Content = db.paginaSet1.Where(x => x.menu == "Inicio").ToList();
            return View();
        }

        public ActionResult Coffee(int categoria = -1, int page = 1, int pageSize = 9)
        {
            List<producto> productos = null;

            if (categoria == -1)
            {
                productos = db.productoSet.Where(p => p.tipo == "cafe").ToList();
                ViewBag.tituloCategoria = "Todos";
            }
            else if (categoria == 0)
            {
                productos = db.productoSet.Where(p => p.tipo == "cafe" && p.categoria.Count == 0).ToList();
                ViewBag.tituloCategoria = "Ninguna";
            }
            else
            {
                productos = db.productoSet.SqlQuery("SELECT * FROM dbo.productocategoria pc " +
                    "JOIN dbo.productoSet p ON p.id = pc.producto_id " +
                    "WHERE pc.categoria_id = @p0", categoria).ToList();

                ViewBag.tituloCategoria = db.categoriaSet.SqlQuery("SELECT * FROM dbo.categoriaSet WHERE Id = @p0", categoria).ElementAt(0).nombre;
            }

            PagedList<producto> pageProductos = new PagedList<producto>(productos, page, pageSize);

            ViewBag.categorias = db.categoriaSet.ToList<categoria>();
            ViewBag.categoria = categoria;
            return View(pageProductos);
        }

        public ActionResult Arte(int page = 1, int pageSize = 6)
        {
            List<producto> productos = db.productoSet.Where(p => p.tipo == "arte").ToList();
            PagedList<producto> pageProductos = new PagedList<producto>(productos, page, pageSize);

            return View(pageProductos);
        }

        public ActionResult Bocado()
        {
            List<producto> productos = db.productoSet.Where(p => p.tipo == "bocado").ToList();
            return View(productos);
        }

        public ActionResult DescripcionProducto(int id)
        {
            producto producto = db.productoSet.Single(p => p.id == id);
            return View(producto);
        }

    }
}