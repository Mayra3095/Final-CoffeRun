﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_CoffeeRun;
using Proyecto_CoffeeRun.Models;
using System.Data.Entity;
using System.Data;
using System.Net;
using PagedList;
using System.IO;

namespace Proyecto_CoffeeRun.Controllers
{
    public class ProductoController : Controller
    {
        public ModelCoffeeRunContainer db = new ModelCoffeeRunContainer();

        // GET: Productos
        public ActionResult Index(int page = 1, int pageSize = 9)
        {
            List<producto> productos = db.productoSet.ToList();
            PagedList<producto> pageProductos = new PagedList<producto>(productos, page, pageSize);

            return View(pageProductos);
        }

        public ActionResult Crear()
        {
            List<categoria> categorias = db.categoriaSet.ToList<categoria>();
            ViewBag.categorias = new MultiSelectList(categorias, "Id", "nombre", null);
            categorias.Insert(0, new categoria { Id = -1, nombre = "Ninguna" });

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear([Bind(Include = "id,nombre,descripcion,precio,tipo,imagen")] producto producto, FormCollection form)
        {
            if (!form["Categorias"].Split(',')[0].Equals("-1"))
            foreach (string i in form["Categorias"].Split(','))
            {
                Int32 num = Int32.Parse(i);
                producto.categoria.Add(db.categoriaSet.Single(c => c.Id == num));
            }

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = producto.id + Path.GetExtension(file.FileName);

                    string ruta = "";

                    switch (producto.tipo)
                    {
                        case "cafe": ruta = "variedades-cafe"; break;
                        case "arte": ruta = "arte-cafe"; break;
                        case "bocado": ruta = "bodado"; break;
                    }

                    producto.imagen = fileName;
                    var path = Path.Combine(Server.MapPath("~/Content/img/" + ruta + "/"), fileName);
                    file.SaveAs(path);

                }
            }

            if (producto.imagen == null) producto.imagen = "#";

            if (ModelState.IsValid)
            {
                db.productoSet.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(producto);
        }

        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            producto producto = db.productoSet.Find(id);

            if (producto == null)
            {
                return HttpNotFound();
            }

            string[] selectedValues = new string[producto.categoria.Count];

            for (int i = 0; i < producto.categoria.Count; i++)
            {
                selectedValues[i] = producto.categoria.ElementAt(i).nombre.ToString();
            }

            List<categoria> categorias = db.categoriaSet.ToList<categoria>();
            categorias.Insert(0, new categoria { Id = -1, nombre = "Ninguna" });

            ViewBag.categorias = new MultiSelectList(categorias, "Id", "nombre", new string[] { "1" });
            ViewBag.selectedValues = selectedValues;

            return View(producto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "id,nombre,descripcion,precio,tipo,imagen")] producto producto, FormCollection form)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = producto.id + Path.GetExtension(file.FileName);

                    string ruta = "";

                    switch (producto.tipo)
                    {
                        case "cafe": ruta = "variedades-cafe"; break;
                        case "arte": ruta = "arte-cafe"; break;
                        case "bocado": ruta = "bodado"; break;
                    }

                    producto.imagen = fileName;
                    var path = Path.Combine(Server.MapPath("~/Content/img/" + ruta + "/"), fileName);
                    file.SaveAs(path);

                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();

                if (form["Categorias"].Count() > 0)
                {
                    db.Database.ExecuteSqlCommand("DELETE FROM dbo.productocategoria WHERE producto_id = " + producto.id);

                    if (!form["Categorias"].Split(',')[0].Equals("-1"))
                    foreach (string i in form["Categorias"].Split(','))
                    {
                        db.Database.ExecuteSqlCommand("INSERT INTO dbo.productocategoria VALUES(" + producto.id + ", " + i + ")");
                    }
                }
                
                return RedirectToAction("Index");
            }

            return View(producto);
        }

        public ActionResult Eliminar(int id){
            db.Database.ExecuteSqlCommand("DELETE FROM dbo.productocategoria WHERE producto_id = " + id + ";");
            producto producto = db.productoSet.Find(id);
            db.productoSet.Remove(producto);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}